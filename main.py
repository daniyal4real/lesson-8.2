# # Множественное наследование + делигирование
#
# class Doctor:
#
#     def __init__(self, degree):
#         self.degree = degree
#
#     def cure(self):
#         print("я лечу пациентов")
#
#
#     def graduate_from_university(self):
#         print("Я отучился и стал доктором")
#
#
# class Firefighter:
#
#     def __init__(self, title):
#         self.title = title
#
#     def extinguish(self):
#         print("я тушу пожары")
#
#
#     def graduate_from_university(self):
#         print("Я отучился и теперь я пожарником")
#
#
# class Specialist(Doctor, Firefighter):
#
#     def __init__(self, degree, title):
#         super().__init__(degree)
#         Firefighter.__init__(self, title)
#
#     def graduate_from_university(self):
#         print("Кем я стал после учебы?")
#         super().graduate_from_university()
#
#
#
#
# employee = Specialist("leitenant", "PhD")
# print(employee.degree)
# print(employee.title)


# Метакласс
# class Point:
#     MAX = 100
#     MIN =  0
#
#
# def create_class_point(name, base, attrs):
#     attrs.update({'MAX': 100, 'MIN': 0})
#     return type(name, base, attrs)
#
# class Point(metaclass=create_class_point):
#     def get_data(self):
#         return (0, 0)
#
# point1 = Point()
# print(point1.MAX)
# print(point1.MIN)
# print(point1.get_data())


class Meta(type):
    # # way 1
    # def __init__(cls, name, base, attrs):
    #     super().__init__(name, base, attrs)
    #     cls.MAX = 100
    #     cls.MIN = 0


    def __new__(cls, name, base, attrs):
        attrs.update({'MAX': 100, 'MIN': 0})
        return type.__new__(cls, name, base, attrs)


class Point(metaclass=Meta):
    def get_data(self):
        return (0, 0)


point1 = Point()
print(point1.MAX)
print(point1.MIN)
print(point1.get_data())
